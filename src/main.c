#include <genesis.h>
#include "resources.h"

// each states has 3 phases. An init phase, a Update phase and an exit phase.
typedef enum {
	PhaseInit,
	PhaseUpdate,
	PhaseExit,
} state_phase_id;

// action_id list all the available action types for every actors. These request
// are then mapped to actor specific state definition. They must stay 
// decoupled.
typedef enum {
	ActionIdle,
	ActionMoveRight,
	ActionMoveLeft,
	ActionJump,
	// all states for all actor can be defined here without harm
} action_id;

typedef void (*action_input_fn)(const actor_state_t* self, struct actor_t* a, const action_id action);
typedef void (*state_fn)(const actor_state_t* self, struct actor_t* a, const state_phase_id phase);

// actor_state_t describes a state for an actor.
typedef struct {
	action_input_fn on_action;
	state_fn on_update;

	// animation frames, an other properties can defined here
} actor_state_t;

// actor_t defines a moving object on screen. Actors are best described as 
// being `alive`. 
typedef struct {
	bool is_on_floor;
	V2f32 pos;
	V2f32 velocity;

	void (*update_fn)(struct actor_t* a);

	actor_state_t* actor_state;
	actor_state_t* next_state;
} actor_t;

// the possible states for a player actor
typedef enum {
	PlyrStateIdle = 0,
	PlyrStateWalk,
	PlyrStateJump,
	PlyrStateFall,
	PlyrState__COUNT,
} player_state_id;

actor_state_t player_state[PlyrState__COUNT] = {
	[PlyrStateIdle] = {.on_action=_idle_on_action, .on_update=_idle_update},
	[PlyrStateWalk] = {.on_action=_walk_on_action, .on_update=_walk_update},
	[PlyrStateJump] = {.on_action=_jump_on_action, .on_update=_jump_update}
};

static inline 
void ScheduleStateChange(actor_t* a, player_state_id next) {
	// @fabio: change this to something more versatile if you wish to implement
	// this pattern to your enemies too.
	if(a->next_state == NULL) {
		a->next_state = &player_state[next];
	}
}

static inline 
void EmitAction(actor_t *a, action_id action) {
	if(a->actor_state) {
		a->actor_state->on_action(a->actor_state, a, action);
	}
}

static inline 
void DoStateMaintenance(actor_t *a) {
	if(a->next_state) {
		if(a->actor_state) {
			a->actor_state->on_update(a->actor_state, a, PhaseExit);
		}
		a->actor_state = a->next_state;
		a->actor_state->on_update(a->actor_state, a, PhaseInit);
		a->next_state = NULL;
	}

	if(a->actor_state) {
		a->actor_state->on_update(a->actor_state, a, PhaseUpdate);
	}
}

static inline
void UpdateActor(actor_t* a) {
	if(a->update_fn) {
		a->update_fn(a);
	}
}

// [ Player Idle State ] -------------------------------------------------------

void _idle_on_action(const actor_state_t* self, actor_t* a, const action_id action) {
	switch(action) {
	case ActionJump:
		if(a->is_on_floor) {
			ScheduleStateChange(a, PlyrStateJump);
		}
		break;
	case ActionMoveRight:
		ScheduleStateChange(a, PlyrStateWalk);
		break;
	case ActionMoveLeft:
		ScheduleStateChange(a, PlyrStateWalk);
		break;
	}
}

void _idle_update(const actor_state_t* self, actor_t* a, const state_phase_id phase) {
	switch(phase) {
	case PhaseInit:
		a->velocity.x = FIX32(0);
		// set a timer
		break;
	case PhaseExit: 
		break;
	case PhaseUpdate:
		if(!a->is_on_floor) {
			ScheduleStateChange(a, PlyrStateFall);
		}
		// when the timer is done, change the animation for a character that is 
		// getting bored to wait after the player.
		break;
	}
}

// [ Player Walk State ] -------------------------------------------------------

void _walk_on_action(const actor_state_t* self, actor_t* a, const action_id action) {
	switch(action) {
	case ActionJump:
		if(a->is_on_floor) {
			ScheduleStateChange(a, PlyrStateJump);
		}
		break;
	case ActionMoveRight:
		a->velocity.x = FIX32(1);
		break;
	case ActionMoveLeft:
		a->velocity.x = FIX32(-1);
		break;
	case ActionIdle:
		ScheduleStateChange(a, PlyrStateIdle);
		break;
	}
}

void _walk_update(const actor_state_t* self, actor_t* a, const state_phase_id phase) {
	switch(phase) {
	case PhaseInit:
		break;
	case PhaseExit: 
		break;
	case PhaseUpdate:
		if(!a->is_on_floor) {
			ScheduleStateChange(a, PlyrStateFall);
		}
		break;
	}
}

void _update_player(actor_t* a) {
	u16 joy_1 = JOY_readJoypad(JOY_1);
	
	// You could use a Recorded input event log or even as input parameter of 
	// this function...
	//u16 joy_1 = ReadDemoInput();

	if(joy_1 & BUTTON_C) {
		EmitAction(a, ActionJump);
	} 
	
	if(joy_1 & BUTTON_LEFT) {
		EmitAction(a, ActionMoveLeft);
	}

	if(joy_1 & BUTTON_RIGHT) {
		EmitAction(a, ActionMoveRight);
	}

	if(joy_1 == 0) {
		EmitAction(a, ActionIdle);
	}

	bool is_on_floor = a->pos.y > FIX32(128);
	if(is_on_floor) {
		a->pos.y = FIX32(128);
		a->velocity.y = FIX32(0);
	} else {
		// integrate velocity to the position.
		a->pos.x += a->velocity.x;
		a->pos.y += a->velocity.y;
		// apply gravity
		a->velocity.y += FIX32(1); 
	}
	a->is_on_floor = is_on_floor;

	DoStateMaintenance(a);
}


int main() {
	VDP_drawText("Hello World!", 4, 4);
	// Fromt he moon example
	VDP_setPalette(PAL1, moon.palette->data);
	VDP_drawImageEx(VDP_PLAN_A, &moon, TILE_ATTR_FULL(PAL1, 0, 0, 0, 1), 12, 12, 0, CPU);
	
	actor_t player = {
		.pos=(V2f32) {.x=FIX32(0), .y=FIX32(0)},
		.velocity=(V2f32) {.x=FIX32(0), .y=FIX32(0)},
		.actor_state=&player_state[PlyrStateIdle],
		.next_state=NULL,
		.is_on_floor=TRUE,
		.update_fn=_update_player,
	};

	while(TRUE) {
		VDP_waitVSync();
		JOY_update();
		UpdateActor(&player);
	}
	return 0;
}
