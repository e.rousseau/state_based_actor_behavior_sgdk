#!/bin/bash

RELEASE=./out.bin
DEBUG=./out.bin


# run the emulator in half full_screen mode
if [ -f $RELEASE ] ; then
	blastem "$RELEASE" 1920
else
	blastem "$DEBUG" 1920
fi
